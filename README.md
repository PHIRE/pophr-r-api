# PopHR API for R

R Interface to communicate with a PopHR server. It can be used to query both its data and knowledge. We built this interface for users to use PopHR's data in new and innovative ways.

* Perform advanced analysis
* Visualize its data in innovative ways
* Connect its data with other available sources
* and many more

## Getting Started

Access your favorite PopHR instance; [click here](https://pophr.mchi.mcgill.ca) for the public version. Once you have a registered account, download and look at the ``vignettes/exploration.Rmd`` file in this repository. Using the RStudio IDE will make this much easier.

From there, feel free to look at the package's documentation.

## Step by Step Instruction to Use PopHR API

1. In pophr-r-api page, under "Project" tab, click the download icon to download this repository
2. Unzip the downloaded file, shorten the folder name if necessary
3. Double click on the "pophrpkg.Rproj" file to open the pophr project using RStudio
4. In the panel of "Environment, History...", click on the tab "Build",
5. Click on "Install and Restart" and waite until the process is done
6. Open the ``vignettes/exploration.Rmd`` file in this repository to explore the functions and datasets in this package

## Collaboration

This library does not at the moment accept merge requests, but suggestions or collaborations are welcome. Please communicate with us using the email address in the PopHR web application.

## Authors

* **Ben Willetts** - *Dev Team*
* **Maxime Lavigne** - *Dev Team*
* **Mengru Yuan** - *Scientific Team*

## License

The copyright to this library belongs to McGill university and the MCHI research group. Users are free to use or modify this code in non-commercial ways. Use this code at your own risk. This statement will be refined later.
