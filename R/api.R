library(dplyr)
library(rowr)
library(magrittr)


#' Lists Public Health Concepts known to this PopHR instance
#'
#' @param connection A valid PopHR connection object.
#' @param contain Characters within a concept
#' @return Tibble of 3-tuples <concept, en, fr> where concept is the short form ID.
#' @export
#'
#' @examples
#'   concepts <- ListConcepts(Connect(Endpoints("CA")))
listconcepts <- function(connection) {
  concepts <- doGetConcepts(connection)

  tibble::tibble(
    concept = concepts,
    en = connection$resolveAll(concepts, "en"),
    fr = connection$resolveAll(concepts, "fr")
  )
}
ListConcepts <- function(connection, contain = "all") {
  concepts = listconcepts(connection)
  if (c("all") %in% contain) {
    concepts = concepts
  } else {
    concepts = filter(concepts, grepl(paste(contain, collapse = "|"),en, ignore.case = TRUE))
  }
  return(concepts)
}


#' Lists Public Health Concepts based on the central concept known to this PopHR instance
#'
#' @param connection A valid PopHR connection object.
#' @param central The ID of a PH Concept
#' @return Tibble of 3-tuples <concept, en, fr> where concept is the short form ID.
#' @export
#'
#' @examples
#'   concepts.related.diabete <- ListRelatedConcepts(Connect(Endpoints("CA")), central = "purl:DOID_9352")
ListRelatedConcepts <- function(connection, central = NULL) {
  concepts = listconcepts(connection)
  relations = GetCausalrelations(connection)
  if (is.null(central)) {
    relations = relations
  } else {
    relations = relations %>% filter(src == central | dst %in% central) %>%
      mutate(concept.central = central,
             concept.central.en = head(ifelse(src == central, src.en, dst.en),1),
             concept.related = ifelse(src == central, dst, src),
             direction = ifelse(src == central, "dst", "src"),
             concept.related.en = ifelse(src == central, dst.en, src.en),
             concept.related.fr = ifelse(src == central, dst.fr, src.fr)) %>%
      select(concept.central,concept.central.en,concept.related, concept.related.en, concept.related.fr, direction)
  }
  return(relations)
}


#' List indicators for a specific public health concept
#'
#' @param connection A valid PopHR connection object.
#' @param concept The IDs of PH Concepts
#' @param contain Characters within a indicator
#' The tible returned will have the following columns
#' concept   -> the associated concept
#' isDefault -> Is this indicator, the "default" indicator for this PH concept.
#' indicator -> Indicator ID
#' en        -> English locale indicator name
#' fr        -> French locale indicator name
#' highestResolution -> Highest (most resolute) geo resolution for which data is available
#' numOfYearsSuppressed -> Number of years we suggest, not to display (ie. burn-in)
#'
#' @return A Tible of indicators
#' @importFrom magrittr %>%
#' @importFrom rlang .data
#' @import dplyr
#' @import stringr
#' @export
#'
#' @examples
#' indicators = ListIndicators(Connect(Endpoints("CA")), concept = "phont:Smoking")
listindicators <- function(connection, concept) {
  res <- doGetIndicatorsForConcept(connection, concept)

  if (!("displayUnit" %in% names(res))) {
    #cat(paste0("displayUnit not found for concept : ", concept, "\n"))
    return(tibble::tibble())
  }
  if (!("sourceUnit" %in% names(res))) {
    #cat(paste0("sourceUnit not found for concept : ", concept, "\n"))
    return(tibble::tibble())
  }
  if (!("numOfYearsSuppressed" %in% names(res))) res$numOfYearsSuppressed = rep(NA, nrow(res))
  if (!("highestResolution" %in% names(res))) res$highestResolution = rep(NA, nrow(res))

  # Get all the direct attributes
  res = dplyr::select(res, -.data$data, -.data$displayUnit)
  # Put back an exploded source unit
  res = dplyr::bind_cols(dplyr::select(res, -.data$sourceUnit), res$sourceUnit)

  res = res %>%
    # Adding back the associated PH concept.
    dplyr::mutate(concept = concept) %>%
    # Renaming the indicator column for readability
    dplyr::rename(indicator = .data$id, unit = .data$ref, unit_type = .data$type) %>% #unit_conv = conversionFactor: error of no such col
    # Adding Locales
    dplyr::mutate(
      en = connection$resolveAll(.data$indicator, "en"),
      fr = connection$resolveAll(.data$indicator, "fr"))

  # Format as a tibble (and provide a readable ordering of columns)
  tibble::as_tibble(res[, c(
    "concept", "isDefault", "indicator", "en", "fr",
    "highestResolution", "unit", "unit_type", "numOfYearsSuppressed" #"unit_conv": error of no such col
  )])
}
ListIndicators <- function(connection, concept = "all", contain = NULL){
  if(c("all") %in% concept){
    concepts = ListConcepts(connection)$concept
  } else {
    concepts = concept
  }
  indicator.list = concepts %>% map_dfr(~ listindicators(connection, .))
  if (is.null(contain)) {
    indicator.list = indicator.list
  } else {
    indicator.list = filter(indicator.list, grepl(paste(contain, collapse = "|"), en, ignore.case = TRUE))
  }
  return(indicator.list)
}

#' List dimensions you can use with this instance.
#'
#' @param connection A valid PopHR connection object.
#'
#' @return A Tible of dimensions
#' @importFrom magrittr %>%
#' @importFrom rlang .data
#' @export
#'
#' @examples
#' dimensions = ListDimensions(Connect(Endpoints("CA")))
ListDimensions <- function(connection) {
  dims <- doGetDimensions(connection)
  purrr::map_dfr(names(dims),
          function(k) dims[[k]] %>% dplyr::mutate(dim_type = k)) %>%
    dplyr::rename(dim = .data$ref) %>%
    dplyr::mutate(
      en = connection$resolveAll(dim, "en"),
      fr = connection$resolveAll(dim, "fr"))%>%
    dplyr::select(-.data$codeList) %>%
    dplyr::filter(.data$isUserSelectable) %>%
    dplyr::select(-.data$isUserSelectable)
}


#' Gets the code list for a specific dimensions ie. 2-tuple<code, value>
#'
#' @param connection valid PopHR connection object.
#' @param dimension ID of a dimension
#'
#' @return Tibble of <code, value>
#' @importFrom magrittr %>%
#' @importFrom rlang .data
#' @import purrr
#' @export
#'
#' @examples
#' GetDimensionCodes(Connect(Endpoints("CA")), "age")
GetDimensionCodes <- function(connection, dimension) {
  dims <- doGetDimensions(connection)
  (purrr::map_dfr(names(dims), function(k) dims[[k]]) %>%
      dplyr::filter(.data$ref == dimension))[1,]$codeList
}


#' Generate the dimension table content
#'
#' @param connection A valid PopHR connection object.
#' @param limito dimension to limit the output to
#' @param outputas Format of the output
#'
#' @return A table lists the ontology name of each dimensions
#' @importFrom magrittr %>%
#' @export
#'
#' @examples
#' geo.dims = ListDimsContent(Connect(Endpoints("CA")))
ListDimsContent <- function(connection, limito = c("all", "geo", "age", "sex"), outputas = c("table", "list")) {
  # check input values
  limito <- match.arg(limito)
  if (!limito %in% c("all", "geo", "age", "sex")) stop(print(limito))
  outputas <- match.arg(outputas)
  if (!outputas %in% c("table", "list")) stop(print(outputas))

  dims <- doGetDimensions(connection)
  dims <- purrr::map_dfr(names(dims), function(k) dims[[k]] %>%  dplyr::mutate(dim_type = k)) %>%  dplyr::rename(dim = .data$ref) %>%  dplyr::filter(.data$isUserSelectable == TRUE)

  if (limito == "all") {
    dims = dims
  } else{
    dims = dims[dims$dim_type == limito,]
  }

  t = list()
  for (i in 1:length(dims$dim)) {
    codename = dims$dim[i]
    code = paste(codename, "code", sep = '_')
    t[[codename]][["name"]] = dims$codeList[[i]]$value
    t[[codename]][["code"]] = dims$codeList[[i]]$code
  }
  noms = names(t)

  if (outputas == "list") {
    return(t)
  } else {
    # transform the nested list to a table
    t = lapply(t, function(x) x = x$name)
    noms = names(t)
    t = do.call(rowr::cbind.fill, c(t, fill = NA))
    t = data.frame(lapply(t, as.character), stringsAsFactors = FALSE)
    colnames(t) = connection$resolveAll(noms, "en")
    return(t)
  }
}


#' Gets the data in an "unresolved way", means ID and codes
#'
#' "unresolved way" means the columns with stratification, are still using the codes and not their name.
#' "resolved way" means the columns with stratification, are using their names and not the codes
#'
#' @param connection valid PopHR connection object.
#' @param indicator The ID of an indicator
#' @param query Query configuration (see further for details)
#'
#' @return A data cube
#' @export
#'
#' @examples
#' cube <- GetUnresolvedData(Connect(Endpoints("CA")), indicator = "phio:DiabetesPrevalenceIndicator",
#' query = list("d" = "phio:stratification_CanadaHealthRegion"))
GetUnresolvedData <- function(connection, indicator, query) {
  return(doGetIndicator(connection, indicator, query))
}


#' Gets the data in an "resolved way", means readable name of region and groups
#'
#' @param connection valid PopHR connection object.
#' @param indicator The ID of an indicator
#' @param query Query configuration (see further for details)
#'
#' @return A data cube
#' @export
#'
#' @examples
#' cube <- GetResolvedData(Connect(Endpoints("CA")), indicator = "phio:DiabetesPrevalenceIndicator",
#' query = list("d" = "phio:stratification_CanadaHealthRegion"))
GetResolvedData <- function(connection, indicator, query) {
  out <- tryCatch(
    {
      unresolvedf = doGetIndicator(connection, indicator, query)
      doMappingCodelist(connection,unresolvedf)
    },
    error = function(cond) {
      message(paste("data not available"))
      message("make sure there is a correct query input")
      message("please use function \"ListIndicatorDimension\" to check potential indicator and query inputs")
      # Choose a return value in case of error
      return(NA)
    }
  )
  return(out)
}


#' List indicators for a specific public health concept
#'
#' @param connection A valid PopHR connection object.
#' @param concept The IDs of PH Concepts
#' @param indicators the IDs of indicators
#' @return A Tible of indicators
#' @importFrom magrittr %>%
#' @importFrom rlang .data
#' @export
#'
#' @examples
#' indicators.dim = ListIndicatorDimensionsFromConcept(Connect(Endpoints("CA")),
#' concept="phont:Smoking")
listindicatordimensionsfromconcept <- function(connection, concept) {
  res <- doGetIndicatorsForConcept(connection,  concept)

  if (rlang::is_empty(res) == FALSE) {
    # modify the unseeable data resolution
    dim = c("phio:stratification_1yr_age_groups", "phio:stratification_CT", "phio:stratification_male_female")
    dim.rep = paste(dim, collapse = ", ")
    dim = gsub("CT", "CLSC", dim)
    dim = gsub("1yr", "5yr", dim)
    dim = combAll(dim, separator = "|")
    dim = paste(dim, gsub("CLSC", "RSS", dim, fixed = TRUE), gsub("CLSC", "RLS", dim, fixed = TRUE), sep = "|")
    dim = paste(dim, gsub("5yr", "10yr", dim, fixed = TRUE), gsub("5yr_age_groups", "children_adults_elderly", dim, fixed = TRUE), sep = "|")


    # modify the result table
    res = res %>%
      dplyr::select(.data$id, .data$data,) %>%
      dplyr::mutate(concept = concept) %>%
      dplyr::rename(indicator = .data$id) %>%
      dplyr::mutate(dimension = lapply(.data$data, function(t) {if(all(is.na(t))) NA else
          paste(lapply(t$dimensions, function(x) paste(x, collapse = ",")), collapse = "|")})) %>%
      dplyr::mutate(dimension = gsub(dim.rep, dim, .data$dimension)) %>%
      dplyr::mutate(dimension = strsplit(as.character(.data$dimension), "|", fixed = TRUE)) %>%
      tidyr::unnest(.data$dimension) %>%
      dplyr::filter(!.data$dimension == "") %>%
      dplyr::mutate(dimension = ifelse(.data$dimension == "NA" ,NA,.data$dimension)) %>%
      dplyr::mutate(filter.geo.dim = ifelse(grepl("CLSC", .data$dimension), "RLS or RSS",
                                            ifelse(grepl("RLS", .data$dimension), "RSS",
                                                   ifelse(grepl("HealthRegion", .data$dimension), "ProvinceOrTerritory", NA))))
  } else {
    res = data.frame(concept = concept,
                     indicator = NA,
                     dimension = NA,
                     filter.geo.dim = NA,
                     stringsAsFactors = FALSE)
  }

  # Format as a tibble (and provide a readable ordering of columns)
  tibble::as_tibble(res[!duplicated(res), c("concept", "indicator","dimension", "filter.geo.dim")])
}
ListIndicatorDimension <- function(connection, concept = "all", indicators = "all"){
  if(c("all") %in% concept) {
    concepts = ListConcepts(connection)$concept
  } else {
    concepts = concept
  }
  indicator.list.dim = concepts %>% map_dfr(~ listindicatordimensionsfromconcept(connection, .))
  if (!c("all") %in% indicators) {
    indicator.list.dim = filter(indicator.list.dim, grepl(paste(indicators, collapse = "|"), indicator, ignore.case = TRUE))
  }

  return(indicator.list.dim)
}

#' Generate query from the table elements
#'
#' @param dimension A string that has the stratification dimensions.
#' @param filter A string that has the filter dimensions
#'
#' @return A query consists of a list of strings
#' @export
#'
#' @examples
#' \dontrun{
#' ind.dim = ListIndicatorDimensionsFromConcept(Connect(Endpoints("CA")), concept="phont:Smoking")
#' GetQueryFromTbl(dimension = ind.dim$dimension[1])}
GetQueryFromTbl <- function(dimension = NULL, filter = NULL) {
  if (grepl(",", dimension)) {
    query.mod = as.list(strsplit(dimension, split = ",", fixed = TRUE)[[1]])
  } else{
    query.mod = dimension
  }
  query = stats::setNames(query.mod,  c(rep("d", length(query.mod))))
  query = c(query, "f" = filter)
  return(query)

}


#' Generate the URL for the methodology document for a input indicator
#'
#' @param connection A valid PopHR connection object.
#' @param indicator A pophr indicator id
#' @param language A character specify the language required for the methodology document
#' @return A URL
#' @export
#'
#' @examples
#' GetMethodolgyFile(Connect(Endpoints("CA")),
#' indicator = "phio:DiabetesPrevalenceIndicator", language = "en")
GetMethodolgyFile <- function(connection, indicator, language = "en") {
  paste0(connection$endpoint,"pophr/doc/latest/", indicator, "/", language)
}


#' Generate the causal relationship table for all concepts
#'
#' @param connection A valid PopHR connection object.
#' @return A table contains the concepts id and names in en and fr, as well as the relationship between concepts
#' @importFrom magrittr %>%
#' @export
#'
#' @examples
#' GetCausalrelations(Connect(Endpoints("CA")))
GetCausalrelations <- function(connection) {
  relations = connection$get(paste0("pophr/graph"))[["edges"]]
  relations = relations[(relations$src != "owl:Nothing" & relations$dst != "owl:Nothing"),]
  relations= relations %>%
    dplyr::mutate(src.en = connection$resolveAll(.data$src, "en"),
                  src.fr = connection$resolveAll(.data$src, "fr"),
                  dst.en = connection$resolveAll(.data$dst, "en"),
                  dst.fr = connection$resolveAll(.data$dst, "fr")) %>%
    dplyr::select("src", "src.en", "src.fr", "relation", "dst", "dst.en", "dst.fr")

  return(relations)
}




#' Generate the causal graph for the input concept
#'
#' @param connection A valid PopHR connection object.
#' @param concept A pophr concept id
#' @param language A character specify the language required for the graph label
#' @param edge.arrow.size Edge Arrow Size
#' @param edge.color Edge Color
#' @param vertex.label.font Vertex Label Font
#'
#' @return A plot
#' @importFrom igraph plot.igraph
#' @importFrom igraph graph_from_data_frame
#' @importFrom igraph V
#' @export
#'
#' @examples
#' GetCausalGraph(Connect(Endpoints("CA")) , concept = "phont:Smoking")
GetCausalGraph <- function(connection, concept, language = "en",  edge.arrow.size=.6, edge.color="gray85", vertex.label.font=2 ) {
  relations = GetCausalrelations(connection)
  relations = relations[(relations$src == concept|relations$dst == concept),]

  if (nrow(relations)== 0) {
    print("no causal link available for this concept")
  } else {
    links = relations[,c("src", "dst", "relation")]
    nodes = data.frame(id = unique(c(relations$src, relations$dst)),
                       label.en = unique(c(relations$src.en, relations$dst.en),),
                       label.fr = unique(c(relations$src.fr, relations$dst.fr)))
    net =  igraph::graph_from_data_frame(d = links, vertices = nodes, directed = T)

    if(language == "en"){label = igraph::V(net)$label.en
    }else{
      igraph::V(net)$label.fr}
    return(igraph::plot.igraph(net, edge.arrow.size = edge.arrow.size, vertex.label = label,edge.color=edge.color,
               vertex.label.font= vertex.label.font))
  }
}
